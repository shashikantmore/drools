package com.sample;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

import com.sample.Message;

public class TestApp {
	
	public static void main(final String [] args) {
		
		Message message = new Message();
		
		message.setMessage("Hello");
		message.setStatus(Message.HELLO);
		
		String url = "http://localhost:8080/kie-server/services/rest/server";
		String username = "kieserver";
		String password = "kieserver1!";
		String container = "example";
		String session = "ksession-rules";
		
		
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(url, username, password);
		config.setMarshallingFormat(MarshallingFormat.JSON);
//		Set<Class<?>> allClasses = new HashSet<Class<?>>();
//		allClasses.add(Message.class);
//		config.addExtraClasses(allClasses);

		KieServicesClient client  = KieServicesFactory.newKieServicesClient(config);
		RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
		List<Command<?>> commands = new ArrayList<Command<?>>();
		commands.add((Command<?>) KieServices.Factory.get().getCommands().newInsert(message, "message"));
		commands.add((Command<?>) KieServices.Factory.get().getCommands().newFireAllRules("fire-identifier"));
		commands.add((Command<?>) KieServices.Factory.get().getCommands().newGetObjects("message"));
		BatchExecutionCommand batchCommand = KieServices.Factory.get().getCommands().newBatchExecution(commands,session);
		ServiceResponse<org.kie.api.runtime.ExecutionResults> response = ruleClient.executeCommandsWithResults(container, batchCommand);
		System.out.println(((List<Message>)response.getResult().getValue("message")).size());
		System.out.println(((List<Message>)response.getResult().getValue("message")).get(0).getMessage());
	}

}
